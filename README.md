# LivrariaWeb


##  Atividade em Grupo
Esta atividade deve ser realizada em grupos de no máximo 06 pessoas.
Esta atividade valerá até 2,5 pontos na nota A2.

Requisitos:
    Uma livraria necessita construir uma aplicação Web com as características descritas a seguir.
    Utilize o banco de dados MySQL fornecido no arquivo em bdlivraria.sql, que possui as tabelas 'tbcategorias', 'tblivros' e 'tbusuarios'.
    Elabore um projeto Web em Java utilizando o modo tradicional ou Spring

## Framework contendo:
    1) Uma página Web inicial contendo:
        • os nomes completos de cada aluno e seu RGM;
        • um menu com os links para as páginas criadas.
    2) (0,50) Crie uma página chamada: listaLivrosPorCategoria com um formulário, contendo um campo select para que o usuário selecione uma categoria (Liste as categorias existentes na tabela 'tbcategorias') e um botão. Ao clicar no botão esta página deverá visualizar todos os  ivros da categoria selecionada.
    3) (0,50) Criar uma página que permita cadastrar novos livros na tabela 'tblivros'. Utilize campos de textos para solicitar os dados do novo livro, mas no caso da categoria deverá ser um listbox com a lista de categorias possíveis preenchida com os valores da tabela 'tbcategorias' do banco de dados.
        a. (0,25) Crie um botão neste formulário para excluir algum livro cadastrado (excluir pelo código do livro). Mostrar uma mensagem adequada.
        b. (0,25) Crie um botão neste formulário para alterar algum livro cadastrado (alterar usando o código do livro). Mostrar uma mensagem adequada.

    4) (0,50) Crie uma página mostrarLivros que possua um formulário que permita ao usuário informar o código do Livro e um botão que ao clicar deverá retornar as informações do livro e caso o código não existe mostrar uma mensagem adequada.
    5) (0,50) O projeto deverá estar em um repositório online (Git Hub ou GitLab).
Observações importantes:
        a) A estrutura do banco de dados (nomes de tabelas, campos, BD, tipos de dados etc.) não poderá ser alterada.
        b) É importante que o grupo proteja com sigilo seu trabalho para evitar anulação da nota por apresentação de trabalhos semelhantes.
        c) Apresentar o trabalho finalizado para o professor avaliar enviando pelo Blackboard os seguintes itens:
            a) Link do projeto no Git;
            b) Arquivo com o projeto compactado em formato zip;
            c) arquivo PDF contendo o histórico dos commits realizados pelo grupo.

## Importar o Projeto + o Apache TomCat

Esta forma de Importação é mais prática do que Zipar o projeto, pois ela inclui todos os arquivos e também nosso servidor Apache TomCat 

```
1º Abra seu Eclipse
2º Clique no botão "File > Import"
3º Pesquise por "WAR" e selecione a opção "War File"
4º Em "War file:" procure o arquivo que se encontra no repositório chamado "ProjetoLivraria.war"
5º De um "Next" e habilite a caixa do "mysql-connector-java-8.0.21.jar"
6º CLique em "Finalizar"
```

