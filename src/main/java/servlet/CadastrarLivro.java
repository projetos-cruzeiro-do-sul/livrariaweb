package servlet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import servlet.Livro;

public class CadastrarLivro {
	
	public void CadastrarLivro(Livro livro) {
		ConnectionFactory obj_DB_Connection = new ConnectionFactory();
		Connection connection = obj_DB_Connection.get_connection();
		String sql = "INSERT INTO bdlivraria.tblivros " 
				+ "(titulo, autor, categoria, valor)" 
				+ " values(?,?,?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			// setar os valores
			stmt.setString(1, livro.getTitulo());
			stmt.setString(2, livro.getAutor());
			stmt.setString(3, livro.getCategoria());
			stmt.setString(4, livro.getValor());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}