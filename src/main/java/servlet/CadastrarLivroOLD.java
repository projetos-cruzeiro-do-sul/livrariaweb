package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class CadastrarLivro
 */
@WebServlet("/CadastrarLivroOLD")
public class CadastrarLivroOLD extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CadastrarLivroOLD() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	
	
	public void CadastrarLivroOLD(Livro livro) {
		ConnectionFactory obj_DB_Connection = new ConnectionFactory();
		Connection connection = obj_DB_Connection.get_connection();
		String sql = "INSERT INTO bdlivraria.tblivros " 
		+ "(titulo, autor, categoria, valor)" 
		+ " values(?,?,?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, livro.getTitulo());
			stmt.setString(2, livro.getAutor());
			stmt.setString(3, livro.getCategoria());
			stmt.setString(4, livro.getValor());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}	
}
	