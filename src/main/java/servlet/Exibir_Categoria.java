package servlet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Exibir_Categoria {
	
	public List<Categoria> getListaCategoria(){
		ConnectionFactory obj_DB_Connection = new ConnectionFactory();
		Connection connection = obj_DB_Connection.get_connection();
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List<Categoria> listaCategorias = new ArrayList<Categoria>();
		
		try {
			String query = "SELECT * FROM tbcategorias;";
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			
			while(rs.next()){
				Categoria categoria = new Categoria();
				categoria.setCategoria(rs.getString("categoria"));
				listaCategorias.add(categoria);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaCategorias;			
	}
}