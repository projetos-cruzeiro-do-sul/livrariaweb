package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/PesquisaLivro")
public class PesquisaLivro extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PesquisaLivro() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String codLivro = request.getParameter("codLivro");
		
		List<Livro> list = getPesquisarLivro(codLivro);
		request.setAttribute("list", list);
		getServletContext().getRequestDispatcher("/mostrarLivros.jsp").forward(request, response);
	}
	
	public List<Livro> getPesquisarLivro(String codLivro){
		ConnectionFactory obj_DB_Connection = new ConnectionFactory();
		Connection connection = obj_DB_Connection.get_connection();

		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List<Livro> list = new ArrayList<Livro>(); 
		
		try {
			String query = "SELECT * FROM bdlivraria.tblivros WHERE codlivro = '" + codLivro + "';";
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();			
			
			while(rs.next()){
				Livro livro = new Livro();
				
				livro.setCodlivro(rs.getInt("codlivro"));
				livro.setTitulo(rs.getString("titulo"));
				livro.setAutor(rs.getString("autor"));
				livro.setCategoria(rs.getString("categoria"));
				livro.setValor(rs.getString("valor"));
				
				list.add(livro);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;			
	}
}
