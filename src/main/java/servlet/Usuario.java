package servlet;

public class Usuario {
	private long rgm;
	private String nome;
	private String senha;
	
	public long getRgm() {
		return rgm;
	}
	public void setRgm(long rgm) {
		this.rgm = rgm;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}	
}
