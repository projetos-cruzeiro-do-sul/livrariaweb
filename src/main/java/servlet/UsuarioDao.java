package servlet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDao {
	ConnectionFactory obj_DB_Connection = new ConnectionFactory();
	Connection connection = obj_DB_Connection.get_connection();

	public void adicionar(Usuario usuario) {
		String sql = "insert into bdlivraria.tbusuarios; " + "(nome,senha)" + " values(?,?)";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, usuario.getNome());
			stmt.setString(2, usuario.getSenha());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Usuario> getLista() {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List<Usuario> listaUsuarios = new ArrayList<Usuario>();
		
		try {			
			String query = "select * from bdlivraria.tbusuarios;";
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Usuario usuario = new Usuario();
				usuario.setRgm(rs.getLong("rgm"));
				usuario.setNome(rs.getString("nome"));
				usuario.setSenha(rs.getString("senha"));
				listaUsuarios.add(usuario);
			}			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return listaUsuarios;
	}
	
	public void alterar(Usuario usuario) {
		String sql = "update bdlivraria.tbusuarios; set nome=?, email=?, endereco=?,"+"DataNascimento=? where id=?";
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, usuario.getNome());
			stmt.setString(2, usuario.getSenha());
			stmt.execute();
			stmt.close();
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public void remover(Usuario usuario) {
		try {
			PreparedStatement stmt = connection.prepareStatement("delete "+"from bdlivraria.tbusuarios; where id=?");
			stmt.setLong(1, usuario.getRgm());
			stmt.execute();
			stmt.close();
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
}
