<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
		integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<title>Cadastrar Livros</title>
</head>

<body>
	<%@ page import="servlet.*, java.util.*"%>
    
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="index.jsp">Livraria</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="listaLivrosPorCategoria.jsp">Categoria <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="cadastrarLivro.jsp">Cadastrar Livro <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="MostrarLivros.jsp">Mostrar Livros <span class="sr-only">(current)</span></a>
				</li>
			</ul>
		</div>
	</nav><br>
	<div class="container">
	    	<%
		    	String titulo = request.getParameter("titulo");
		    	String autor = request.getParameter("autor");
		    	String valor = request.getParameter("valor");
		    	String categoria = request.getParameter("categoria");
		    	
		    	Livro livro = new Livro();
		    	livro.setTitulo(titulo);
		    	livro.setAutor(autor);
		    	livro.setCategoria(categoria);
		    	livro.setValor(valor);

		    	try{
					CadastrarLivro cadastrar;
					cadastrar = new CadastrarLivro();
					cadastrar.CadastrarLivro(livro);
			%>
			<div class="alert alert-success" role="alert">
			  Cadastro Realizado com Sucesso!
			</div>
			<%
		    	}catch(Exception e){
		    		//out.print("Erro no cadastro do usu�rio, volte para a p�gina de Cadastro!" + e);
		    		out.println("<div class=\"alert alert-danger\" role=\"alert\">");
		    		out.println("Erro no cadastro do usu�rio, volte para a p�gina de Cadastro!");
		    		out.println("</div>");
		    	}
			%>
	</div>
</body>
</html>