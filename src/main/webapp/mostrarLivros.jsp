<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
			integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<meta charset="UTF-8">
	<title>Mostrar Livros</title>
</head>
<body>
	<%@ page import="servlet.*, java.util.*"%>
    
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
		
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="index.jsp">Livraria</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>		
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="listaLivrosPorCategoria.jsp">Categoria <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="cadastrarLivro.jsp">Cadastrar Livro <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="mostrarLivros.jsp">Mostrar Livros <span class="sr-only">(current)</span></a>
				</li>
			</ul>
		</div>
	</nav>
	<br>

	<div class="container">
		<div class="input-group">
			<form class="PesquisaLivro" method="post">
			  <div class="form-row">
			    <div class="col">
			      <label for="inputZip">Mostrar Livros</label>
			      <input type="text" class="form-control" placeholder="Digite o Id" name="codLivro">
			    </div>
			  </div>
			  <button class="btn btn-primary" type="submit">Buscar</button>
			</form>
		    
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Código</th>
			      <th scope="col">Título</th>
			      <th scope="col">Autor</th>
			      <th scope="col">Categoria</th>
			      <th scope="col">Valor</th>
			    </tr>	    
			  </thead>
			  <tbody>
			  	<%
			  		String erro = "Livro não encontrado!";
			    	PesquisaLivro pesquisaLivro = new PesquisaLivro();
			    	List<Livro> listLivro = pesquisaLivro.getPesquisarLivro(request.getParameter("codLivro"));
			    		  
			    	try {
			    		
				    	Iterator<Livro> it_listLivro = listLivro.iterator();
				    	while(it_listLivro.hasNext()){
				    		Livro livro = new Livro();
				    		livro = it_listLivro.next();		    		  
			    %>	
			    <tr>
			      <th scope="row"><%= livro.getCodlivro()%></th>
			      <td><%=livro.getTitulo() %></td>
			      <td><%=livro.getAutor() %></td>
			      <td><%=livro.getCategoria() %></td>
			      <td><%=livro.getValor() %></td>
			    </tr>
			    <%
				    	}
		    			
			    	} catch (Exception e) {			    		
			    		//e.printStackTrace();
			    		//out.println("<div class=\"alert alert-danger\" role=\"alert\">Livro não encontrado!</div>");
			    		out.println("<div class=\"alert alert-danger\" role=\"alert\">Livro não encontrado!</div>: " + e.getMessage());
			    	}
			    %>
			  </tbody>
			</table>
		</div>
	</div>
</body>
</html>